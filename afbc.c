#define _GNU_SOURCE
#include <assert.h>
#include <fcntl.h>
#include <gbm.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/random.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES3/gl3.h>

bool setup_gl(int device) {
    char *path;
    asprintf(&path, "/dev/dri/renderD%i", device);

    int32_t fd = open(path, O_RDWR);
    free(path);

    if (fd == -1)
        return false;

    struct gbm_device *gbm = gbm_create_device(fd);
    EGLDisplay egl_dpy = eglGetPlatformDisplay(EGL_PLATFORM_GBM_MESA, gbm, NULL);
    eglInitialize(egl_dpy, NULL, NULL);
    static const EGLint config_attribs[] = {
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES3_BIT_KHR,
        EGL_NONE
    };
    EGLConfig cfg;
    EGLint count;
    eglChooseConfig(egl_dpy, config_attribs, &cfg, 1, &count);
    eglBindAPI(EGL_OPENGL_ES_API);
    static const EGLint attribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 3,
        EGL_NONE
    };
    EGLContext core_ctx = eglCreateContext(egl_dpy,
                                           cfg,
                                           EGL_NO_CONTEXT,
                                           attribs);
    eglMakeCurrent(egl_dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, core_ctx);

    const char *vendor = (const char*)glGetString(GL_VENDOR);

    return vendor && !strcmp(vendor, "Panfrost");
}

int main(int argc, char *argv[])
{
    if (argc == 2 && strcmp(argv[1], "device") == 0) {
        for (int i = 128; i < 256; ++i) {
            if (setup_gl(i)) {
                printf("%i\n", i);
                return 0;
            }
        }
        return 7;
    }

    if (argc < 5) return 1;

    int device = strtoul(argv[1], NULL, 0);

    if (!setup_gl(device)) {
        fprintf(stderr, "device %i is not panfrost", device);
    }

    bool read;
    if (strcmp(argv[2], "read") == 0)
        read = true;
    else if (strcmp(argv[2], "write") == 0)
        read = false;
    else
        return 2;

    int pixz = strtoul(argv[3], NULL, 0);
    int type = strtoul(argv[4], NULL, 0);
    bool ytr = false;
    if (type & 0x100000) {
        type ^= 0x100000;
        setenv("PAN_AFBC_YTR", "1", 1);
    }
    if (type & 0x200000) {
        type ^= 0x200000;
        ytr = true;
    }
    argv += 4;

    int pixtype = GL_UNSIGNED_BYTE;

    bool zs = false;

    enum {p_none, p_565, p_5a1, p_z24s8} pack_mode = p_none;
    if (type == GL_UNSIGNED_SHORT_5_6_5) {
        pixtype = type;
        type = GL_RGB;
        pack_mode = p_565;
    } else if (type == GL_UNSIGNED_SHORT_5_5_5_1) {
        pixtype = type;
        type = GL_RGBA;
        pack_mode = p_5a1;
    } else if (type == GL_DEPTH24_STENCIL8) {
        pixtype = GL_UNSIGNED_INT_24_8;
        type = GL_DEPTH_STENCIL;
        pack_mode = p_z24s8;
        zs = true;
    }

    GLuint fboId;
    glGenFramebuffers(1, &fboId);
    glBindFramebuffer(GL_FRAMEBUFFER, fboId);

    GLuint fbotexId;
    glGenTextures(1, &fbotexId);
    glBindTexture(GL_TEXTURE_2D, fbotexId);

    int size = 256 * pixz * (zs ? 2 : 1);
    uint8_t *data = calloc(size, 1);

    if (read) {
        if (argc != 16*pixz+5)
            return 5;

        ++argv;
        for (int y = 4; y < 8; ++y)
            for (int x = 4; x < 8; ++x)
                if (ytr) {
                    int d[4];
                    for (int c = 0; c < pixz; ++c)
                        d[c] = strtoul(*(argv++), NULL, 0);

                    int sum = d[0];
                    int rv = d[1] - 256;
                    int bv = d[2] - 256;

                    int green = (sum*4 - rv - bv + 3) / 4;
                    data[(16*y+x)*pixz+0] = rv + green;
                    data[(16*y+x)*pixz+1] = green;
                    data[(16*y+x)*pixz+2] = bv + green;

                    if (pixz == 4)
                        data[(16*y+x)*pixz+3] = d[3];
                } else switch (pack_mode) {
                    case p_565: {
                        uint16_t packed;
                        packed = (strtoul(*(argv++), NULL, 0) >> 3) << 11;
                        packed |= (strtoul(*(argv++), NULL, 0) >> 2) << 5;
                        packed |= (strtoul(*(argv++), NULL, 0) >> 3);
                        *(uint16_t *)(data+(16*y+x)*2) = packed;
                        break;
                    }
                    case p_5a1: {
                        uint16_t packed;
                        packed = (strtoul(*(argv++), NULL, 0) >> 3) << 11;
                        packed |= (strtoul(*(argv++), NULL, 0) >> 3) << 6;
                        packed |= (strtoul(*(argv++), NULL, 0) >> 3) << 1;
                        packed |= (strtoul(*(argv++), NULL, 0) >> 7);
                        *(uint16_t *)(data+(16*y+x)*2) = packed;
                        break;
                    }
                    case p_z24s8: {
                        uint32_t packed;
                        packed = (strtoul(*(argv++), NULL, 0) & 0xffffff) << 8;
                        packed |= (strtoul(*(argv++), NULL, 0) & 0xff);
                        *(uint32_t *)(data+(16*y+x)*4) = packed;
                        break;
                    }
                    case p_none:
                        for (int c = 0; c < pixz; ++c)
                            data[(16*y+x)*pixz+c] = strtoul(*(argv++), NULL, 0);
                        break;
                    }
    } else {
        int len = strtoul(argv[1], NULL, 0);
        if (len + 6 != argc)
            return 6;
        argv += 2;
        uint8_t *d = calloc(4096, 1);
        uint32_t *head = (uint32_t *)d;
        head[0] = 64;
        head[1] = len/8;
        d += head[0];
        unsigned bit = 1;
        unsigned i = 0;
        while (*argv) {
            d[i] |= bit * strtoul(*(argv++), NULL, 0);
            bit *= 2;
            if (bit == 256) {
                bit = 1;
                ++i;
            }
        }
        char *str;
        asprintf(&str, "%p", (void *)head);
        setenv("PAN_AFBC_TEXTURE", str, 1);
        free(str);
    }

    glTexImage2D(GL_TEXTURE_2D, 0, type, 16, 16, 0, type, pixtype, data);

    if (zs) {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, fbotexId, 0);

        glReadPixels(0, 0, 16, 16, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, data);
    } else {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbotexId, 0);

        glReadPixels(0, 0, 16, 16, type, GL_UNSIGNED_BYTE, data);
    }

    if (read) {
        char *loc = getenv("PAN_AFBC_LOC");
        if (!loc)
            return 3;
        uint8_t *afbc = (uint8_t *)strtoul(loc, NULL, 0);
        uint32_t *header = (uint32_t *)afbc;
        afbc += header[0];

        int size = header[1] & 63;
        if (size == 1) {
            fprintf(stderr, "could not compress\n");
            return 4;
        }
        for (int i = 0; i < size; ++i)
            for (int bit = 1; bit < 256; bit *= 2)
                printf("%i ", (afbc[i] & bit)?1:0);
    } else {
        for (int y = 4; y < 8; ++y)
            for (int x = 4; x < 8; ++x)
                if (ytr) {
                    int d[4];
                    for (int c = 0; c < pixz; ++c)
                        d[c] = data[(16*y+x)*pixz+c];

                    int sum = (d[0] + d[1]*2 + d[2]) / 4;
                    unsigned rv = (d[0] - d[1]) + 256;
                    unsigned bv = (d[2] - d[1]) + 256;
                    printf("%i %i %i ", sum, rv, bv);
                    if (pixz == 4)
                        printf("%i ", d[3]);
                } else if (zs) {
                    printf("%i ",
                           (data[(16*y+x)*4+1]) |
                           (data[(16*y+x)*4+2] << 8) |
                           (data[(16*y+x)*4+3] << 16)
                        );
                    printf("%i ", data[(16*y+x)*4]);
                } else {
                    for (int c = 0; c < pixz; ++c)
                        printf("%i ", data[(16*y+x)*pixz+c]);
                }
    }

    puts("");
}
