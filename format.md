# AFBC format

This is a description of the lossless AFBC compression format as used
in v5 and v6 (late Midgard, early Bifrost) Mali GPUs. The extent of
changes compared to this for v7 and later versions is unknown.

## Header format

Header blocks are all before the data blocks.  There is a 16-byte
header block for each 16x16 superblock.

In tiled layout, groups of 4x4 or 8x8 superblocks are stored together,
in scan-line order. TODO are header blocks also tiled?

The first four bytes are a little-endian offset to the superblock from
the start of the texture. TODO what about block-split layout?

The next 96 bits is split into 16 six-bit fields, holding the size in
bytes of each 4x4 block.  The field being 0 indicates a "copy-block" -
the data is the same as in the previous 4x4 block.  The field being 1
indicates the uncompressed storage mode - the data is stored not
compressed, tiled or colourspace transformed.

The 4x4 blocks are in this order (so coord (4, 4) in the texture will
be in block 0):

    2 3 4 5
    1 0 7 6
    e f 8 9
    d c b a

## YTR

YTR is a colourspace transform similar to YUV, but implementable with
just bit shifts and addition/subtraction.

For an 8-bit input, the Y channel is 8 bits, and V and U are 9 bits.
Channels are ordered Y, V (red projection), then U (blue projection).

Encoding:

    Y = (R + G*2 + B) / 4
    V = R - Y + 256
    U = B - Y + 256

Decoding:

It's possible not every valid encoded value is decoded correctly by
this, but all "canonical" values should decode fine.

    R = G + V - 256
    G = (Y*4 - U - V + 512 + 3) / 4
    B = G + U - 256

## Depth/stencil formats

Z24S8 seems to be the same as RGBA8, but with the depth stored in RGB
(LSB in red) and the stencil in alpha.

## Format for 4x4 blocks

Every 4x4 block is compressed individually.

Bits are filled starting with the LSB of the first byte, then second
byte etc, and are tightly packed.

Each section is stored for each channel in turn - there is the
four-bit header for every channel, then the 2x2 bit-size for every
channel etc.

Regions are in scan-line order.

### Data format

For data stored in groups of four for 2x2 or pixel regions, the format
for storing values is:

For 0-bit data, nothing is stored.

For 1-bit data, there is a bit for each of the four regions covered by
the data.

For anything else, there is a two-bit swizzle, then interleaved data
for three regions (i.e. the bits r0[0], r1[0], r2[0], r0[1], ...)  The
swizzle stores the region number for the fourth region, which is zero.

It is possible, though not useful, to have values with more bits than
can fit in the channel.  Values can overflow and wrap around.

### Data

For each channel, there is a header with the bit-size needed for the
channel.  All one bits means that the channel is constant - every
pixel in the block has the same value.  The header size is dependent
on the channel size - 5 bits uses three bits, 6-8 uses four.

For every non-constant channel, there are four two-bit twos-complement
signed values which are added to the channel bit-size to get the
number of bits for each 2x2 block.  Negative values are invalid.

For each N-bit channel, there is a N-bit value which is added to all
of the values for that channel.

For every channel with a non-zero bit-size, there are values of that
size for each 2x2 block (see the "Data format" section), which are
added to every value in that 2x2 block.

For each 2x2 block: For every channel with a non-zero bit-size for
that 2x2 block, there are values of that size for each pixel (see the
"Data format" section).

## TODO

- Subsampling
- 4x4 block orders for 32x8, 64x4 and 32x8_64x4 superblocks
- Superblock content hints
- Solid colour blocks - do these store the colour in the header?
- RGB565 YTR
- RGB5A1
- number of header bits for more channel sizes
