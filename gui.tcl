#!/bin/sh
# Tcl ignores the next line -*- tcl -*- \
exec tclsh "$0" -- "$@"

package require Tk

wm title . AFBC

grid [ttk::frame .c -padding "3 3 12 12"] -column 0 -row 0 -sticky nwes
grid columnconfigure . 0 -weight 1; grid rowconfigure . 0 -weight 1

grid [ttk::combobox .c.fmt -state readonly -values {RED RG RGB RGBA YTR YTRA YTRX YTRXA 565 YTR565 5A1 Z24S8} -width 5] -column 2 -row 1
.c.fmt set RED

set ::fmt_chan 0
set ::fmt_enum 0x1903

proc update_fmt {} {
    switch [.c.fmt get] {
        RED { set chan 1; set enum 0x1903 }
        RG { set chan 2; set enum 0x8227 }
        RGB { set chan 3; set enum 0x1907 }
        RGBA { set chan 4; set enum 0x1908 }
        YTR { set chan 3; set enum 0x101907 }
        YTRA { set chan 4; set enum 0x101908 }
        YTRX { set chan 3; set enum 0x301907 }
        YTRXA { set chan 4; set enum 0x301908 }
        565 { set chan 3; set enum 0x8363 }
        YTR565 { set chan 3; set enum 0x108363 }
        5A1 { set chan 4; set enum 0x8034 }
        Z24S8 { set chan 2; set enum 0x88F0 }
    }
    if {$chan!=$::fmt_chan || $enum!=$::fmt_enum} {
        change_fmt $chan
        set ::fmt_chan $chan
        set ::fmt_enum $enum
    }
}

grid [ttk::button .c.to -text "Conv to" -command conv_to] -column 2 -row 2 -sticky s
grid [ttk::button .c.from -text "Conv from" -command conv_from] -column 2 -row 3 -sticky n

grid [ttk::spinbox .c.bytes -from 0 -increment 1 -to 63 -textvariable e_num_bytes \
          -command bytes_update -width 3] \
    -column 2 -row 4

set e_num_bytes 7

set w 4
set h 4

grid [ttk::frame .c.pix] -column 4 -row 1 -sticky nwe -rowspan 4

proc gn_l {x y} { join [list .c.pix. $x _ $y] "" }
proc gn_t {x y c} { join [list .c.pix.i_ $x _ $y _ $c] "" }
proc gn_v {x y c} { join [list ::gn_v_ $x _ $y _ $c] "" }

proc setc {x y r g b a} {
    catch { [gn_l $x $y] configure -background [format #%02x%02x%02x $r $g $b] }
}

proc getc {x y} {
    set z $::fmt_chan
    for {set c 0} {$c<$z} {incr c} {
        lappend a [expr $[gn_v $x $y $c]]
    }
    for {set c $z} {$c<4} {incr c} {
        lappend a 0
    }
    return $a
}

proc setc_chan {x y c new} {
    set rgb [getc $x $y]
    lset rgb $c $new
    setc $x $y {*}$rgb
}

proc setc_cell {x y} {
    set rgb [getc $x $y]
    setc $x $y {*}$rgb
}

proc ent_check {wid new} {
    set $new [scan $new %d]
    if {$new == ""} { set new 0 }
    if {![string is integer $new] || $new < 0 } { return 0 }
    set spl [split $wid _]
    lassign $spl _ x y c

    setc_chan $x $y $c $new
    return 1
}

ttk::style configure C0.TEntry -fieldbackground #ffdddd
ttk::style configure C1.TEntry -fieldbackground #ddffdd
ttk::style configure C2.TEntry -fieldbackground #ccccff
ttk::style configure C3.TEntry -fieldbackground #ffffff

proc entstyle {c} { join [list C $c .TEntry] "" }

for {set y 0} {$y<$h} {incr y} {
    for {set x 0} {$x<$w} {incr x} {
        grid [ttk::label [gn_l $x $y] -width 3 -background #000000] -column $x -row $y -in .c.pix -pady 1
    }
}

proc change_fmt {new} {
    for {set y 0} {$y<$::h} {incr y} {
        for {set x 0} {$x<$::w} {incr x} {
            if {$new<$::fmt_chan} {
                for {set c $new} {$c<$::fmt_chan} {incr c} {
                    destroy [gn_t $x $y $c]
                }
            } else {
                for {set c $::fmt_chan} {$c<$new} {incr c} {
                    grid [ttk::entry [gn_t $x $y $c] -width 3 \
                              -textvariable [gn_v $x $y $c] \
                              -style [entstyle $c] \
                              -validate key -validatecommand {ent_check %W %P}] \
                        -column $x -row [expr $y+$::h*($c+1)] -in .c.pix
                    set [gn_v $x $y $c] 0
                }
            }
        }
    }
}
update_fmt

bind TCheckbutton <Left> { tk::TabToWindow [tk_focusPrev %W] }
bind TCheckbutton <Right> { tk::TabToWindow [tk_focusNext %W] }

bind . n conv_to

proc conv_to {} {
    update_fmt

    set w $::w
    set h $::h
    set z $::fmt_chan

    set bits [expr $::num_bytes*8]

    for {set i 0} {$i<$bits} {incr i} {
        lappend c [expr $[in_v $i]]
    }

#    puts    [list ./afbc $::device write $z $::fmt_enum $bits {*}$c]
    set val [exec ./afbc $::device write $z $::fmt_enum $bits {*}$c]

    for {set y 0} {$y<$h} {incr y} {
        for {set x 0} {$x<$w} {incr x} {
            for {set c 0} {$c<$z} {incr c} {
                set v [lindex $val [expr ($y*$w+$x)*$z+$c]]
                set [gn_v $x $y $c] $v
            }
            setc_cell $x $y
        }
    }
}

grid [ttk::frame .c.in] -column 1 -row 1 -sticky nwe -rowspan 3

proc in_c {i} { join [list .c.in. $i] "" }
proc in_v {i} { join [list ::in_v_ $i] "" }

proc bytes_update {} {
    if {$::e_num_bytes==$::num_bytes
        || $::e_num_bytes==0} { return }
    set_num_bytes $::e_num_bytes
    conv_to
}

proc inpad {x y} {
    if {$x%8==7} {
        return "1 1 5"
    } else {
        return "1"
    }
}

set num_bytes 0

proc set_num_bytes {bytes} {
    set bits [expr $bytes*8]
    set old [expr $::num_bytes*8]
    if {$bits==$old} { return }
    if {$bits<$old} {
        for {set i $bits} {$i<$old} {incr i} {
            destroy [in_c $i]
        }
        set ::num_bytes $bytes
        return
    }

    for {set i $old} {$i<$bits} {incr i} {
        set x [expr $i%32]
        set y [expr $i/32]

        set [in_v $i] 0
        grid [ttk::checkbutton [in_c $i] -variable [in_v $i] -padding [inpad $x $y] -command conv_to] \
            -column $x -row [expr $y+1] -in .c.in
    }
    set ::num_bytes $bytes
}

for {set x 0} {$x<32} {incr x; incr x} {
    grid [ttk::label [join [list .c.in.l_ $x] ""] -text $x] \
        -column $x -row 0 -in .c.in
}

set_num_bytes 7

bind . o conv_from

proc conv_from {} {
    update_fmt

    set w $::w
    set h $::h
    set z $::fmt_chan

    for {set y 0} {$y<$h} {incr y} {
        for {set x 0} {$x<$w} {incr x} {
            for {set ch 0} {$ch<$z} {incr ch} {
                lappend c [expr $[gn_v $x $y $ch]]
            }
        }
    }

#    puts    [list ./afbc $::device read $z $::fmt_enum {*}$c]
    set ret [exec ./afbc $::device read $z $::fmt_enum {*}$c]

    set bits [llength $ret]
    set bytes [expr $bits/8]

    set_num_bytes $bytes
    set ::e_num_bytes $bytes

    for {set i 0} {$i<$bits} {incr i} {
        set [in_v $i] [lindex $ret $i]
    }
}

bind TEntry f {
    set name [split %W _]
    lassign $name _ x y c

    set value [expr $[gn_v $x $y $c]]

    for {set y 0} {$y<$h} {incr y} {
        for {set x 0} {$x<$w} {incr x} {
            set [gn_v $x $y $c] $value
            setc_chan $x $y $c $value
        }
    }

    conv_from
}

set device [exec -ignorestderr ./afbc device]
